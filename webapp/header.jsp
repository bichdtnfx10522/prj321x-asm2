<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<link rel="stylesheet"
	href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css"
	integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l"
	crossorigin="anonymous">
<link href="./css/css.css" rel="stylesheet" type="text/css" />
</head>
<body>
	<header class="bg-primary">
		<div class="row ">
			<div class="col-3">
				<h1>PRM321x</h1>
				<p>Wellcome my website</p>
			</div>
			<div class="col-9 row">

				<form class="form-inline ml-auto " action="SearchSevlet"
					method="post">
					<input class="form-control " type="search" placeholder="Search"
						aria-label="Search" id="keyword" name = "key">
					<button class="btn text-white" type="submit">Search</button>
				</form>

			</div>
		</div>
		<nav class="nav topnav" style="background-color: #000fff;">
			<a class="text-white" href="#">Home</a> <a class="text-white"
				href="#">Products</a> <a class="text-white" href="#">About us</a> <a
				class="text-white" class="ml-auto">Login</a>
		</nav>

	</header>
</body>
</html>