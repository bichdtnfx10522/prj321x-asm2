package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import model.Account;

/**
 * Servlet implementation class LoginServlet
 */
@WebServlet("/LoginServlet")
public class LoginServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public LoginServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */

	protected void processRequest(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		response.setContentType("text/html;charset=UTF-8");
		request.setCharacterEncoding("utf-8");

		// Du lieu truyen tu form
		String userID = request.getParameter("username");
		String password = request.getParameter("password");

		List<Account> list = new ArrayList<Account>();
		Account acc = new Account("nb@gmail.com", "123", 1, "Ngoc Bich", "Hai Duong", "0936432291", 0);
		Account acc1 = new Account("nb1@gmail.com", "123", 2, "Ngoc Bich", "Hai Duong", "0936432291", 0);

		list.add(acc);
		list.add(acc1);

		boolean login = false;
		for (Account account : list) {
			if (userID != null && password.equals(account.getPwf()) && userID.equalsIgnoreCase(account.getUsr())) {
				login = true;
				if (account.getRole() == 1) {
					response.sendRedirect("admin/index.jsp");
				} else {
					response.sendRedirect("home.jsp");
				}
			}
		}
		if (login == false) {
			RequestDispatcher rd = request.getRequestDispatcher("login.jsp");
			response.getWriter().println("<p style='color:red;'>User orr password is invalid</p>");
			rd.include(request, response);
		}

	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		RequestDispatcher rd = request.getRequestDispatcher("login.jsp");
		rd.include(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		processRequest(request, response);
	}

}
